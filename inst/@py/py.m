## Copyright (C) 2016 Mike Miller
## Copyright (C) 2023 Vipul Cariappa
## Copyright (C) 2023 Colin B. Macdonald
## SPDX-License-Identifier: GPL-3.0-or-later
##
## This file is part of Octave Pythonic.
##
## Octave Pythonic is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave Pythonic is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave Pythonic; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {} {} py
## @deftypefnx {} {} py.@var{pyname}
## Get the value of a Python object or call a Python function.
## @end deftypefn

function p = py ()
  p = class (struct (), "py");
endfunction

%!assert (py.math.sqrt (2), sqrt (2))
%!assert (ischar (char (py.sys.version)))

%!test
%! assert (isobject (py.int (0)))
%! assert (isa (py.int (0), "pyobject"))

%!test
%! assert (isobject (py.int (2^100)))

## Cannot use '@' to make a handle to a Python function
%!xtest
%! fn = @py.math.abs;
%! assert (fn (-3), 3)
