## SPDX-License-Identifier: BSD-3-Clause
## Copyright (C) 2019-2021 Mike Miller
## Copyright (C) 2019, 2023 Colin B. Macdonald
## Copyright (C) 2023 Vipul Cariappa
##
## .gitlab-ci.yml - GitLab CI configuration for octave-pythonic

image: gnuoctave/octave:latest

stages:
  - build
  - test
  - release-prep
  - release

variables: &variables
  CC: gcc
  CXX: g++
  CXXFLAGS: "-g -O2 -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wall -Wdate-time -Wextra -Wformat=2"
  DEBIAN_FRONTEND: "noninteractive"
  DOCTEST_GIT_REPOSITORY: "https://github.com/gnu-octave/octave-doctest.git"
  DOCTEST_GIT_TAG: "v0.8.0"
  DOCTEST_ROOT: "/tmp/doctest"
  LIBSSL_DEV: "libssl-dev"
  PYENV_GIT_REPOSITORY: "https://github.com/pyenv/pyenv.git"
  PYENV_GIT_TAG: "v2.3.17"
  PYENV_ROOT: "/tmp/pyenv"
  PYTHON_CONFIGURE_OPTS: "--enable-shared"

build:
  stage: build
  script:
    - apt-get update && apt-get install -y git
    - mkdir -p pkg
    - make O=pkg dist dist-zip
  artifacts:
    paths:
      - pkg/*.tar.gz
      - pkg/*.zip

.test:pyenv:
  stage: test
  script:
    - apt-get update && apt-get install -y blt-dev g++ gcc git libbz2-dev libdb-dev
      libffi-dev libgdbm-dev liblzma-dev libncursesw5-dev libreadline-dev libsqlite3-dev
      $LIBSSL_DEV tk-dev uuid-dev zlib1g-dev
    - git clone $DOCTEST_GIT_REPOSITORY $DOCTEST_ROOT
    - git -C $DOCTEST_ROOT checkout $DOCTEST_GIT_TAG
    - git clone $PYENV_GIT_REPOSITORY $PYENV_ROOT
    - git -C $PYENV_ROOT checkout $PYENV_GIT_TAG
    - export PATH=$PYENV_ROOT/bin:$PATH
    - eval "$(pyenv init --path)"
    - eval "$(pyenv init -)"
    - pyenv install $PYTHON_VERSION_FULL
    - pyenv global $PYTHON_VERSION_FULL
    - mkdir -p python${PYTHON_VERSION}
    - make O=python${PYTHON_VERSION} V=1 all
    - make O=python${PYTHON_VERSION} V=1 check
    - make O=python${PYTHON_VERSION} V=1 doctest OCTAVE_PATH=/tmp/doctest/inst
  dependencies: []
  artifacts:
    paths:
      - python${PYTHON_VERSION}/*.log
    expire_in: 4 weeks

.test:pkg:
  stage: test
  script:
    - apt-get update && apt-get install -y g++ gcc python3-dev
    - octave --eval "pkg install -verbose pkg/octave-pythonic-*.tar.gz"
    - octave --eval "d = pkg ('describe', 'pythonic'); assert (d{:}.name, 'pythonic')"
    - octave --eval "pkg load pythonic; v = pyversion (); assert (v(1), '${PYTHON_VERSION}')"
  dependencies:
    - build

coverage:
  stage: test
  script:
    - apt-get update && apt-get install -y g++ gcc python3-dev
    - pip3 install -U gcovr
    - mkdir -p coverage
    - make O=coverage V=1 all
    - make O=coverage V=1 check
    - bash <(curl -S -s https://codecov.io/bash) -x gcov -s coverage -a "-s $PWD/src -r"
    - gcovr --gcov-executable gcov --print-summary coverage
  variables:
    <<: *variables
    CXXFLAGS: "-g -O0 --coverage"
    PYTHON_VERSION: "3"
  dependencies: []
  coverage: '/^lines: \d+\.\d+/'

python3.7:
  variables:
    <<: *variables
    PYTHON_VERSION: "3.7"
    PYTHON_VERSION_FULL: "3.7.16"
  extends: .test:pyenv

python3.8:
  variables:
    <<: *variables
    PYTHON_VERSION: "3.8"
    PYTHON_VERSION_FULL: "3.8.16"
  extends: .test:pyenv

python3.9:
  variables:
    <<: *variables
    PYTHON_VERSION: "3.9"
    PYTHON_VERSION_FULL: "3.9.16"
  extends: .test:pyenv

python3.10:
  variables:
    <<: *variables
    PYTHON_VERSION: "3.10"
    PYTHON_VERSION_FULL: "3.10.10"
  extends: .test:pyenv

python3.11:
  variables:
    <<: *variables
    PYTHON_VERSION: "3.11"
    PYTHON_VERSION_FULL: "3.11.2"
  extends: .test:pyenv

python3.10-octave6.1.0:
  extends: python3.10
  image: gnuoctave/octave:6.1.0

python3.9-octave7.1.0:
  extends: python3.9
  image: gnuoctave/octave:7.1.0

pkg:python3:
  variables:
    <<: *variables
    PYTHON_VERSION: 3
  extends: .test:pkg


# Upload binaries to "generic" gitlab packages
#
# TODO
#   - port build-aux/release-description-gen.py
binary_upload:
  stage: release-prep
  image: curlimages/curl:latest
  rules:
   - if: $CI_COMMIT_TAG
  script:
    - ls
    - |
      tee release_desc.md <<EOF
      #### Changes in this release

      See [the changelog in the NEWS file](https://gitlab.com/gnu-octave/octave-pythonic/-/blob/$CI_COMMIT_TAG/NEWS.md).


      #### sha256 file hash

      EOF
    # strip the leading v in vx.y.z: don't see how to do this without dotenv
    - export VER=${CI_COMMIT_TAG:1}
    - echo $VER
    - export PKG_LINKNAME="octave-pythonic-$VER.tar.gz"
    - export PKG_FILENAME="octave-pythonic-$VER.tar.gz"
    - echo "Listing the md5sums in the release notes"
    # no pushd, image has sh not bash
    - cd pkg
    - sha256sum $PKG_FILENAME | sed -e "s/^/    /" >> "../release_desc.md"
    - cd ..
    - export PKG_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/octave-pythonic/$VER/$PKG_FILENAME"
    - echo "PKG_LINKNAME=$PKG_LINKNAME" >> release_info.env
    - echo "PKG_URL=$PKG_URL" >> release_info.env
    - echo "PKG_FILENAME=$PKG_FILENAME" >> release_info.env
    # do the upload
    - |
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file pkg/$PKG_FILENAME "$PKG_URL"
    - cat release_desc.md
  artifacts:
    paths:
      - release_desc.md
    reports:
      dotenv: release_info.env
    expire_in: 14 days


# Make the actual release
#
# TODO:
#   - if - $CI_COMMIT_TAG =~ /^v[0-9]+(\.[0-9]+)*/
release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo 'running release_job'
    - echo "Draft of release notes follows (newlines eaten)"
    - cat release_desc.md
    - echo "Just debugging:"
    - echo $PKG_LINKNAME
    - echo $PKG_URL
    - echo $PKG_FILENAME
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: './release_desc.md'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
    milestones:
      - $CI_COMMIT_TAG
    assets:
      links:
        - name: $PKG_LINKNAME
          filepath: "/$PKG_FILENAME"
          url: $PKG_URL
          link_type: "package"


include:
  - template: Code-Quality.gitlab-ci.yml


# MacOS tests
#   - this job runs on "M1" (arm64) macs
#   - its possible to run `arch -x86_64 ...`
#   - `gsed` stuff is Issue #112.
macos_tests:
  image: macos-12-xcode-14
  tags:
    - saas-macos-medium-m1
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: $CI_PROJECT_PATH == "gnu-octave/octave-pythonic"
  stage: test
  needs: []
  variables:
    HOMEBREW_NO_AUTO_UPDATE: 1
    HOMEBREW_NO_INSTALL_CLEANUP: 1
    HOMEBREW_NO_INSTALLED_DEPENDENTS_CHECK: 1
  before_script:
    - sw_vers
    - pwd
    - uname -a
    - uname -m
    - echo $SHELL
    # this Python comes from Homebrew
    - which python
    - python --version
    - pip --version
    # this Python comes from the OS
    - /usr/bin/python3 --version
    - /usr/bin/python3 -m pip --version
    # (and installing Octave updates the Homebrew one)
    - brew --version
    - brew install gnu-sed
    - brew info octave
    - brew install octave
  script:
    - gcc --version
    - clang --version
    - octave --version
    - make help SED=gsed
    - make CXX=clang CXXFLAGS="-std=c++11" SED=gsed
    - make check
    - octave --eval "pkg install -forge doctest"
    - make doctest
    - mkdir -p pkg
    - make O=pkg dist dist-zip
  artifacts:
    paths:
      - src/fntests.log
    when: on_failure


# macOS: one can also use Rosetta to emulate x86_64
#  - arch -x86_64 uname -m
#  - arch -x86_64 /usr/bin/python3 --version
#  - arch -arm64 /usr/bin/python3 --version
# macOS: installing a specific python
#  - curl https://www.python.org/ftp/python/3.11.4/python-3.11.4-macos11.pkg --output python.pkg
#  - sudo installer -pkg python.pkg -target /
#  - /usr/local/bin/python3 --version
#  - arch -x86_64 /usr/local/bin/python3 --version
